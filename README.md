# Palarust

Palarust is a two dimensional parallel lattice Boltzmann fluid flow solver
written in Rust. The parallelism is handled through the great 
[Rayon library](https://crates.io/crates/rayon). The name is 
a pun on the [Palabos library](http://www.palabos.org) which is a 
state of the art open source library for complex flows and beyond. 
Palarust is available on [crates.io](https://crates.io/crates/palarust), 
and API Documentation is available on [docs.rs](https://docs.rs/palarust/0.1.0/palarust/).

### Quick demo

To see what Palarust can currently do, check out the `palarust-demo` directory, which
includes demos of code using Palarust. For example, you can run the simulation of the 
flow past a cylinder and vizualize the result of the screen (thanks to the 
[sdl2 library](https://crates.io/crates/sdl2)).

```
> cd palarust-demo
> cargo run --release -- cylinder
```

For more information on the available demos, try:

```
> cd palarust-demo
> cargo run --release -- --help
```

## License

Palarust is distributed under the terms of both the MIT license and the
Apache License (Version 2.0). See [LICENSE-APACHE](LICENSE-APACHE) for
[LICENSE-MIT](LICENSE-MIT) for details. Opening a pull requests is
assumed to signal agreement with these licensing terms.
