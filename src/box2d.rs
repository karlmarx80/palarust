use point2d::Point2d;
use std::ops::Add;
use std::ops::Sub;
use std::ops::Div;
use std::ops::Mul;
use std::cmp;

struct Box2dBuilder {
    x0: i32,
    x1: i32,
    y0: i32,
    y1: i32,
}

impl Box2dBuilder {
    fn new() -> Self{
        Box2dBuilder{x0: 0, x1: 0, y0: 0, y1: 0}
    }

    fn set_x0(mut self, x_:i32) -> Self{
        self.x0 = x_;
        self
    }

    fn set_x1(mut self, x_:i32) -> Self{
        self.x1 = x_;
        self
    }

    fn set_y0(mut self, y_:i32) -> Self{
        self.y0 = y_;
        self
    }

    fn set_y1(mut self, y_:i32) -> Self{
        self.y1 = y_;
        self
    }

    fn finalize(self) -> Box2d {
        assert!(self.x0 <= self.x1, 
            "x0 must be smaller than x1 but one has x0={:} and x1={:}", self.x0, self.x1);
        assert!(self.y0 <= self.y1, 
            "x0 must be smaller than x1 but one has y0={:} and y1={:}", self.y0, self.y1);
        Box2d{ x0: self.x0, x1: self.x1, y0: self.y0, y1: self.y1, }
    }
}

/// The Box2d structure stores 4 corners of a domain (x0,x1,y0,y1 coordinates)
/// and it is always open on the right (by definition the x1 and y1 are
/// not included into the domain.
#[derive(Debug,Clone,Copy)]
pub struct Box2d
{
    pub x0: i32,
    pub x1: i32,
    pub y0: i32,
    pub y1: i32,
}

impl Box2d {
    pub fn new(x0: i32, x1: i32, y0: i32, y1: i32) -> Box2d {
        Box2dBuilder::new().set_x0(x0).set_x1(x1).set_y0(y0).set_y1(y1).finalize()
    }

    pub fn new_from_usize(x0: usize, x1: usize, y0: usize, y1: usize) -> Box2d {
        Box2dBuilder::new().set_x0(x0 as i32).set_x1(x1 as i32).set_y0(y0 as i32).set_y1(y1 as i32).finalize()
    }

    /// Surface of points in the 2d box (each point has surface 1)
    pub fn size(&self) -> usize {
        ((self.x1-self.x0)*(self.y1-self.y0)) as usize
    }

    /// Weighted surface of the Box2d (corners weight 1/4 and edges 1/2)
    pub fn weighted_size(&self) -> f64 {
        self.enlarge(-1).size() as f64 + 1.0 + 0.5*(self.x1-self.x0-2) as f64 + 0.5*(self.y1-self.y0-2) as f64
    }

    pub fn enlarge(&self, dx: i32) -> Box2d {
        Box2dBuilder::new().set_x0(self.x0-dx).set_x1(self.x1+dx).set_y0(self.y0-dx).set_y1(self.y1+dx).finalize()
    }

    pub fn translate(&self, p: Point2d) -> Box2d {
        *self+p
    }

    pub fn multiply(&self, alpha: i32) -> Box2d {
        Box2dBuilder::new().set_x0(self.x0*alpha).set_x1(self.x1*alpha).set_y0(self.y0*alpha).set_y1(self.y1*alpha).finalize()
    }

    pub fn divide(&self, alpha: i32) -> Box2d {
        *self / alpha
    }

    /// Tests if a Box2d contains a Point2d
    /// 
    /// # Arguments
    ///
    /// * `&self` - A Box2d
    /// * `p`  - A Point2d
    ///
    /// # Example
    ///
    /// ```
    /// extern crate palarust;
    /// use palarust::box2d::Box2d;
    /// use palarust::point2d::Point2d;
    /// fn main () {
    ///     let b = Box2d::new(1, 4, 2, 8);
    ///     let p1 = Point2d::new(2, 3);
    ///     let p2 = Point2d::new(-1, -1);
    ///     
    ///     assert!(b.contains(p1),  "p1 must be contained in box");
    ///     assert!(!b.contains(p2), "p2 must not be contained in box");
    /// }
    /// ```
    pub fn contains(&self, p: Point2d) -> bool {
        p.is_contained(*self)

    }

    /// Computes the intersection of two box2d and returns an option.
    /// 
    /// # Arguments
    ///
    /// * `&self` - A Box2d
    /// * `rhs`  - A Box2d
    ///
    /// # Example
    ///
    /// ```
    /// extern crate palarust;
    /// use palarust::box2d::Box2d;
    /// fn main () {
    ///     let b = Box2d::new(1, 4, 2, 8);
    ///     let b1 = Box2d::new(2, 3, 3, 5);
    ///     let b2 = Box2d::new(-4, 2, -1, 4);
    ///     let b3 = Box2d::new(-4, -2, -4, -1);
    ///     
    ///     let int1 = b.intersection(b1);
    ///     match int1 {
    ///         Some(int1) => assert_eq!(int1, Box2d::new(2, 3, 3, 5)),
    ///         None => panic!("There should be an intersection between the two boxes"),
    ///     }
    ///     assert_eq!(int1, b1.intersection(b));
    ///
    ///     let int2 = b.intersection(b2);
    ///     match int2 {
    ///         Some(int2) => assert_eq!(int2, Box2d::new(1, 2, 2, 4)),
    ///         None => panic!("There should be an intersection between the two boxes"),
    ///     }
    ///     assert_eq!(int2, b2.intersection(b));
    ///
    ///     let int3 = b.intersection(b3);
    ///     match int3 {
    ///         Some(int3) => panic!("There should be an intersection between the two boxes"),
    ///         None => assert!(true, "There is no intersection"),
    ///     }
    ///     assert_eq!(int3, b3.intersection(b));
    ///
    /// }
    /// ```
    pub fn intersection(&self, rhs: Box2d) -> Option<Box2d> {
        let x0 = cmp::max(self.x0, rhs.x0);
        let x1 = cmp::min(self.x1, rhs.x1);
        let y0 = cmp::max(self.y0, rhs.y0);
        let y1 = cmp::min(self.y1, rhs.y1);

        // if no corner is contained in the self box
        if x0 <= x1 && y0 <= y1 {
            // else return the intersecting box
            Some(Box2d::new(x0,x1,y0,y1))
        } else {
            // return None
            None
        }

    }
}


// =============== Basic operation with Boxes ========================= //

/// The equality of two Box2d 
/// 
/// # Arguments
///
/// * `&self` - A Box2d
/// * `&other`  - A Box2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::box2d::Box2d;
/// fn main () {
///     let b = Box2d::new(1, 4, 2, 8);
///     
///     assert_eq!(b, Box2d::new(1, 4, 2, 8));
///     assert!(b != Box2d::new(1, 4, 2, 9), "b is not equal to Box2d::new(1, 4, 2, 9).");
/// }
/// ```
impl PartialEq for Box2d {
    fn eq(&self, other: &Box2d) -> bool {
        (self.x0 == other.x0 && self.x1 == other.x1 && self.y0 == other.y0 && self.y1 == other.y1)
    }
}

/// The addition of a Box2d with a Point2d (in this order)
/// 
/// # Arguments
///
/// * `self` - A Box2d
/// * `rhs`  - A Point2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// use palarust::box2d::Box2d;
/// fn main () {
///     let b = Box2d::new(1, 4, 2, 8);
///     let p = Point2d::new(-1, 4);
///     
///     assert_eq!(b+p, Box2d::new(0, 3, 6, 12) );
/// }
/// ```
impl Add<Point2d> for Box2d {
    type Output = Box2d;

    fn add(self, rhs: Point2d) -> Box2d {
        Box2d::new(self.x0+rhs.x, self.x1+rhs.x, self.y0+rhs.y, self.y1+rhs.y)
    }
}

/// The addition of a Point2d with a Box2d (in this order)
/// 
/// # Arguments
///
/// * `self` - A Point2d
/// * `rhs`  - A Box2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// use palarust::box2d::Box2d;
/// fn main () {
///     let b = Box2d::new(1, 4, 2, 8);
///     let p = Point2d::new(-1, 4);
///     
///     assert_eq!(p+b, b+p);
/// }
/// ```
impl Add<Box2d> for Point2d {
    type Output = Box2d;

    fn add(self, rhs: Box2d) -> Box2d {
        Box2d::new(self.x+rhs.x0, self.x+rhs.x1, self.y+rhs.y0, self.y+rhs.y1)
    }
}

/// The multiplication of a Box2d with a scalar (in this order)
/// 
/// # Arguments
///
/// * `self` - A Box2d
/// * `rhs`  - A i32
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::box2d::Box2d;
/// fn main () {
///     let b = Box2d::new(1, 4, 2, 8);
///     let alpha = 2;
///     
///     assert_eq!(b*alpha, Box2d::new(2, 8, 4, 16));
/// }
/// ```
impl Mul<i32> for Box2d {
    type Output = Box2d;

    fn mul(self, rhs: i32) -> Box2d {
        Box2d::new(self.x0*rhs, self.x1*rhs, self.y0*rhs, self.y1*rhs)
    }
}

/// The subtraction of a Box2d with a Point2d (in this order)
/// 
/// # Arguments
///
/// * `self` - A Box2d
/// * `rhs`  - A Point2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// use palarust::box2d::Box2d;
/// fn main () {
///     let b = Box2d::new(1, 4, 2, 8);
///     let p = Point2d::new(-1, 4);
///     
///     assert_eq!(b-p, Box2d::new(2, 5, -2, 4) );
/// }
/// ```
impl Sub<Point2d> for Box2d {
    type Output = Box2d;

    fn sub(self, rhs: Point2d) -> Box2d {
        Box2d::new(self.x0-rhs.x, self.x1-rhs.x, self.y0-rhs.y, self.y1-rhs.y)
    }
}

/// The division of a Box2d with an i32 (in this order)
/// 
/// # Arguments
///
/// * `self` - A Box2d
/// * `rhs`  - An i32
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// use palarust::box2d::Box2d;
/// fn main () {
///     let b = Box2d::new(1, 4, 2, 8);
///     let alpha = 2i32;
///     
///     assert_eq!(b/alpha, Box2d::new(0, 2, 1, 4) );
/// }
/// ```
impl Div<i32> for Box2d {
    type Output = Box2d;

    fn div(self, rhs: i32) -> Box2d {
        Box2d::new(self.x0/rhs, self.x1/rhs, self.y0/rhs, self.y1/rhs)
    }
}


