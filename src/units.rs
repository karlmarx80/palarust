//! Units module is a hleper to convert between lattice and physical 
//! units. 

use cell;

/// The converter contains the minimal characteristic
/// lengthscales to convert units.
/// It takes the characteristic length and velocity 
/// in physical and lattice units. Also takes the
/// non dimensional Reynolds number Re=u L / nu.
#[derive(Debug)]
pub struct Converter {
	u_phys: f64,
	l_phys: f64,
	u_lb: f64,
	l_lb: usize,
	re: f64,
}

impl Converter {
	pub fn new(u_phys: f64, l_phys: f64, u_lb: f64, l_lb: usize, re: f64) -> Self {
		Converter{u_phys: u_phys, l_phys: l_phys, u_lb: u_lb, l_lb: l_lb, re: re}
	}

	/// Returns the characteristic velocity in lattice units
	pub fn get_u_lb<'a>(&'a self) -> &'a f64 {
		&self.u_lb
	}

	/// Returns the characteristic length in lattice units
	pub fn get_l_lb<'a>(&'a self) -> &'a usize {
		&self.l_lb
	}

	/// Returns the dx=l_phys/l_lb of the simulation
	pub fn get_dx(&self) -> f64 {
		self.l_phys/self.l_lb as f64
	}

	/// Returns the dt=u_lb / u_phys * dx of the simulation
	pub fn get_dt(&self) -> f64 {
		self.u_lb / self.u_phys * self.get_dx() 
	}

	/// Returns the kinematic viscosity in physical units
	pub fn get_nu_phys(&self) -> f64 {
		self.l_phys * self.u_phys / self.re
	}

	/// Returns the kinematic viscosity in lattice units
	pub fn get_nu_lb(&self) -> f64 {
		self.l_lb  as f64 * self.u_lb / self.re
	}

	/// Returns the relaxation frequency in lattice units
	pub fn get_rel_freq(&self) -> f64 {
		1.0 / (self.get_nu_lb() * cell::INV_CS2 + 0.5)
	}

	/// Returns an interation from a time in physical units
	pub fn get_iter(&self, t_phys: f64) -> usize {
		let iter = (t_phys / self.get_dt()).round() as usize;
		match iter {
		    0 => 1usize,
		    _ => iter,
		}
	}

	/// Returns a length in lattice units
	pub fn get_n(&self, l: f64) -> usize {
		(l / self.get_dx()).round() as usize
	}

	/// Returns a tuple of lengths in lattice units
	pub fn get_size(&self, lx: f64, ly: f64) -> (usize, usize) {
		(self.get_n(lx), self.get_n(ly))
	} 
}