pub trait LatticeOps2d {
	fn collide(&mut self);
	
	fn periodic_stream(&mut self);

	fn bulk_stream(&mut self);

	fn bd_stream(&mut self);

	fn collide_and_stream(&mut self) {
		self.collide();
		self.bulk_stream();
		self.bd_stream();
	}
}

pub trait MultiLatticeOps2d : LatticeOps2d {
	fn communicate(&mut self);
}

