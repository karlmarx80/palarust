
use lattice2d::Lattice2d;
use multi_lattice2d::MultiLattice2d;
use box2d::Box2d;
use ind_hlp;
use rayon::prelude::*;
use core::marker::Sync;
use cell;

/// RedProcFun trait contains the methods for reductive processing functionals
pub trait RedProcFunToScalarMl2d : RedProcFun2dInterface + Sync {
	/// Executes in parallel reductive  processing functiona
	fn process(&self, ml: &mut MultiLattice2d) -> f64{
		self.get_app_domains().par_iter().zip(ml.get_mut_lattices().par_iter_mut()).map(
			|(b,l)| {
				match *b {
					Some(b) => {
						self.process_atomic(l, &b)
					},
					None => { 0.0 },
				}
		}).sum()
	}

	/// Atomic lattice execution of the reductive processing functional
	/// on a Lattice2d
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) -> f64;
}

/// ProcFun trait contains the methods for processing functionals
pub trait RedProcFun2dInterface {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>>;
}

/// Reduce the value of a scalar function of the populations
#[derive(Debug)]
pub struct RedScalarFun2d<Fun> where Fun: Sync + Fn(&[f64; cell::Q]) -> f64 {
	app_domains: Vec<Option<Box2d>>,
	foo: Fun,
}

impl <Fun: Sync+Fn(&[f64; cell::Q]) -> f64> RedScalarFun2d<Fun>  {
	pub fn new(foo: Fun, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// -ml.get_env() is needed because we want to reduce only the bulks
			app_domains.push(dom.translate(-loc).intersection(bb.enlarge(-(ml.get_env() as i32))));
		}

		RedScalarFun2d{foo: foo, app_domains: app_domains}
	}
}

impl <Fun: Sync+Fn(&[f64; cell::Q]) -> f64> RedProcFun2dInterface for RedScalarFun2d<Fun> {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}
}

impl <Fun: Sync+Fn(&[f64; cell::Q]) -> f64> RedProcFunToScalarMl2d for RedScalarFun2d<Fun> {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) -> f64 {
		let mut sum = 0.0f64;
		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 
				sum += (self.foo)(lat.get_pops(i));
			}
		}
		sum
	}

}

/// Reduce the value of a scalar function of the populations and the position
#[derive(Debug)]
pub struct RedScalarAndPosFun2d<Fun> where Fun: Sync + Fn(&[f64; cell::Q],i32,i32) -> f64 {
	app_domains: Vec<Option<Box2d>>,
	foo: Fun,
}

impl <Fun: Sync+Fn(&[f64; cell::Q],i32,i32) -> f64> RedScalarAndPosFun2d<Fun>  {
	pub fn new(foo: Fun, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// -ml.get_env() is needed because we want to reduce only the bulks
			app_domains.push(dom.translate(-loc).intersection(bb.enlarge(-(ml.get_env() as i32))));
		}

		RedScalarAndPosFun2d{foo: foo, app_domains: app_domains}
	}
}

impl <Fun: Sync+Fn(&[f64; cell::Q],i32,i32) -> f64> RedProcFun2dInterface for RedScalarAndPosFun2d<Fun> {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}
}

impl <Fun: Sync+Fn(&[f64; cell::Q],i32,i32) -> f64> RedProcFunToScalarMl2d for RedScalarAndPosFun2d<Fun> {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) -> f64 {
		let loc = lat.get_loc();
		let mut sum = 0.0f64;
		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 
				sum += (self.foo)(lat.get_pops(i), ix+loc.x, iy+loc.y);
			}
		}
		sum
	}

}

