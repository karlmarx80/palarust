#![feature(test)]
#![feature(step_by)]
// #![doc(html_playground_url = "https://play.rust-lang.org/")]


// #[macro_use]
extern crate test;
extern crate core;

#[macro_use]
extern crate arrayref;
extern crate image;

extern crate rayon;
// extern crate num_cpus;
extern crate sdl2;

pub mod moments;
pub mod cell;
pub mod multi_lattice2d;
pub mod lattice2d;
pub mod lattice_ops_2d;
pub mod descriptor2d;
pub mod dynamics;
pub mod io;
pub mod point2d;
pub mod box2d;
pub mod communicator;
pub mod ind_hlp;
pub mod fd_hlp2d;
pub mod lbm_hlp2d;
pub mod colormaps;
pub mod proc_fun;
pub mod red_proc_fun;
pub mod units;
pub mod util;


#[macro_use]
#[cfg(test)]
mod benches {
    use test::Bencher;

    const NX: i32 = 500;
    const NY: i32 = 500;

    #[bench]
    fn bench_ml_collide_and_stream_one_proc(b: &mut Bencher) {
        use cell;
        use cell::bulk_dyn::BGK;
        use multi_lattice2d::MultiLattice2d;
        use lattice_ops_2d::LatticeOps2d;

        let omega = 1.9f64;
        let (cx,cy):(i32,i32) = ((NX-1)/2,(NY-1)/2); 
        let sigma = ((NX+NY)/2/10) as f64;

        let _gauss_rho = |_x:i32,_y:i32| -> f64 { 
            1.0 + 0.01*((-(_x as i32-cx)*(_x as i32-cx)-(_y as i32-cy)*(_y as i32-cy)) as f64/(2.0*sigma)).exp()
        };
        let _const_u   = |_x:i32,_y:i32| -> [f64; cell::D] { [0.1, -0.1] };
        let mut lattice = MultiLattice2d::new(
            NX as usize,NY as usize, 1, 1, 1, BGK::new(omega));
        
        b.iter(|| {
            lattice.collide_and_stream();
        } );
    }

    #[bench]
    fn bench_ml_collide_and_stream_two_proc(b: &mut Bencher) {
        use cell;
        use cell::bulk_dyn::BGK;
        use multi_lattice2d::MultiLattice2d;
        use lattice_ops_2d::LatticeOps2d;

        let omega = 1.9f64;
        let (cx,cy):(i32,i32) = ((NX-1)/2,(NY-1)/2); 
        let sigma = ((NX+NY)/2/10) as f64;

        let _gauss_rho = |_x:i32,_y:i32| -> f64 { 
            1.0 + 0.01*((-(_x as i32-cx)*(_x as i32-cx)-(_y as i32-cy)*(_y as i32-cy)) as f64/(2.0*sigma)).exp()
        };
        let _const_u   = |_x:i32,_y:i32| -> [f64; cell::D] { [0.1, -0.1] };
        let mut lattice = MultiLattice2d::new(
            NX as usize,NY as usize, 2, 1, 1, BGK::new(omega));
        
        b.iter(|| {
            lattice.collide_and_stream();
        } );
    }

    #[bench]
    fn bench_ml_collide_and_stream_four_proc(b: &mut Bencher) {
        use cell;
        use cell::bulk_dyn::BGK;
        use multi_lattice2d::MultiLattice2d;
        use lattice_ops_2d::LatticeOps2d;

        let omega = 1.9f64;
        let (cx,cy):(i32,i32) = ((NX-1)/2,(NY-1)/2); 
        let sigma = ((NX+NY)/2/10) as f64;

        let _gauss_rho = |_x:i32,_y:i32| -> f64 { 
            1.0 + 0.01*((-(_x as i32-cx)*(_x as i32-cx)-(_y as i32-cy)*(_y as i32-cy)) as f64/(2.0*sigma)).exp()
        };
        let _const_u   = |_x:i32,_y:i32| -> [f64; cell::D] { [0.1, -0.1] };
        let mut lattice = MultiLattice2d::new(
            NX as usize,NY as usize, 2, 2, 1, BGK::new(omega));
        
        b.iter(|| {
            lattice.collide_and_stream();
        } );
    }

}