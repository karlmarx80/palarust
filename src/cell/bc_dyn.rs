use moments;
use dynamics::{Dynamics,Parameters};
use ind_hlp;
use cell::{D,Q,CS2};

// ======================================================================== //
// ======================== BounceBack implementations ==================== //
// ======================================================================== //

/// Bounce back builder struct
#[derive(Debug,Clone,Copy)]
pub struct BounceBackBuilder(f64);

impl BounceBackBuilder {
    pub fn new() -> Self {
        BounceBackBuilder(0.0)
    }

    pub fn finalize(&mut self) -> BounceBack {
        BounceBack(self.0)
    }

}

/// The BounceBack nodes are boundary nodes where macroscopic moments
/// are correct. They are reflecting 
#[derive(Debug,Clone,Copy)]
pub struct BounceBack(f64);

impl BounceBack {
    pub fn new() -> BounceBack {
        BounceBackBuilder::new().finalize()
    }
}

// ============= implementation of Parameters trait for BounceBack ========= //
impl Parameters for BounceBack {
    fn get_rel_freq<'a>(&'a self) -> &'a f64 {
        &self.0
    }

    fn set_rel_freq(&mut self, omega: f64) {
        self.0 = omega;
    }
}

// ============= implementation of Moments trait for BounceBack ========= //

impl moments::Moments for BounceBack {
    /// Computes the density on a BounceBack node
    /// Since there is no physical behavior on these nodes
    /// rho is set to 1.
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations (dummy unsused variable)
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_rho(&self, _f: &[f64; Q]) -> f64 {
        1.0 // default value
    }

    /// Computes the momentum on a BounceBack node
    /// Since there is no correct physical behavior on these nodes
    /// j is set to [0.0, 0.0]s.
    ///
    /// # Arguments
    ///
    /// * `f` - dummy vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_j(&self, _f: &[f64; Q]) -> [f64; D] {
            [0.0, 0.0]
    }

    /// Computes the momentum on a BounceBack node
    /// Since there is no correct physical behavior on these nodes
    /// P is set to [[0.0, 0.0], [0.0, 0.0]].
    ///
    /// # Arguments
    ///
    /// * `f` - dummy vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_stress(&self, _f: &[f64; Q]) -> [[f64; D]; D] {

         [[0.0, 0.0], [0.0, 0.0]]
    }

    /// Computes the momentum on a BounceBack node
    /// Since there is no correct physical behavior on these nodes
    /// tau is set to [[0.0, 0.0], [0.0, 0.0]].
    ///
    /// # Arguments
    ///
    /// * `f` - dummy vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_deviatoric_stress(&self, _f: &[f64; Q]) -> [[f64; D]; D] {
        [[0.0, 0.0], [0.0, 0.0]]
    }
}

// // ============= implementation of Dynamics trait for BounceBack ========= //

impl  Dynamics for BounceBack {
    /// Computes the equilibrium distribution vector.
    /// The bounce back nodes are not fluid nodes, therefore
    /// they do not possess any equilibrium distribution. 
    ///
    /// # (Dummy) Arguments
    ///
    /// * `rho` - the density
    /// * `u` - the velocity
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_equilibria(&self, _rho: &f64, _u: &[f64; D]) -> [f64; Q] {
        [0.0; Q]
    }

    /// Computes the equilibrium distribution vector.
    /// The bounce back nodes are not fluid nodes, therefore
    /// they do not possess any equilibrium distribution. 
    ///
    /// # (Dummy) Arguments
    ///
    /// * `rho` - the density
    /// * `u` - the velocity
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_non_equilibria(&self, _pi: &[[f64; D]; D]) -> [f64;Q] {
        [0.0; Q]
    }

    /// Computes the collision (in-place f^out from f^in). 
    /// Must conserve mass and momentum. Here the populations
    /// undergo a reflexion.
    ///
    /// # Arguments
    ///
    /// * `f` - mutable f^in state
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn collide(&self, f: &mut [f64; Q]) {
        for i in 1..5 {
            f.swap(i, i+4);
        }
    }
}


// ========================================================================= //
// ======================= VelDirichletDyn struct ========================== //
// ========================================================================= //

/// The builder class for the VelDirichletDyn.
#[derive(Debug,Clone,Copy)]
pub struct VelDirichletDynBuilder<T> {
    base_dyn: T,
    u: [f64; D],
    dir: i32,
    ori: i32,
}


impl <T:Dynamics> VelDirichletDynBuilder<T> {
    pub fn new(base_dyn: T) -> Self {
        VelDirichletDynBuilder{base_dyn: base_dyn, u:[0.0, 0.0], dir: 0, ori: 0}
    }

    pub fn set_u(mut self, u: [f64; D]) -> Self {
        self.u = u;
        self
    }

    pub fn set_dir(mut self, dir: i32) -> Self {
        assert!(dir == 0 || dir == 1, "dir must be 0 or 1 and is {:?}", dir);
        self.dir = dir;
        self
    }

    pub fn set_ori(mut self, ori: i32) -> Self {
        assert!(ori == -1 || ori == 1, "dir must be -1 or 1 and is {:?}", ori);
        self.ori = ori;
        self
    }


    pub fn finalize(self) -> VelDirichletDyn<T> {
        assert!(self.dir == 0 || self.dir == 1, "dir must be 0 or 1 and is {:?}", self.dir);
        assert!(self.ori == -1 || self.ori == 1, "dir must be -1 or 1 and is {:?}", self.ori);
        VelDirichletDyn{base_dyn: self.base_dyn, u: self.u, dir: self.dir, ori: self.ori}
    }
}

/// The velocity Dirichlet dynamics stores the velocity of its node
/// which is supposed to be a boundary 
/// and computes the density depending on the direction of the boundary.
#[derive(Debug,Clone,Copy)]
pub struct VelDirichletDyn<T> {
    base_dyn: T,
    u: [f64; D],
    dir: i32,
    ori: i32,
}

/// Dynamics which stores the velocity. Usefull for Dirichlet boundary conditions.
impl <T:Dynamics> VelDirichletDyn<T> {
    /// By default the velocity is intialized to zero
    pub fn new(base_dyn: T, dir: i32, ori: i32) -> Self {
        VelDirichletDynBuilder::new(base_dyn).set_dir(dir).set_ori(ori).finalize()
    }
}

// ============= implementation of Parameters trait for VelDirichletDyn<T> ========= //
impl <T:Dynamics> Parameters for VelDirichletDyn<T> {
    fn get_rel_freq<'a>(&'a self) -> &'a f64 {
        &self.base_dyn.get_rel_freq()
    }

    fn set_rel_freq(&mut self, omega: f64) {
        self.base_dyn.set_rel_freq(omega);
    }
}

// ============= implementation of Moments trait for VelDirichletDyn<T> ========= //

impl <T:Dynamics> moments::Moments for VelDirichletDyn<T> {
    /// Computes the density from the distribution function 
    /// rho = sum_i f_i
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_rho(&self, f: &[f64; Q]) -> f64 {
        let on_wall_indices = ind_hlp::sub_index(self.dir, 0);

        let normal_indices = ind_hlp::sub_index(self.dir, self.ori);

        let mut rho_on_wall = 0.0;
        for i in on_wall_indices.iter() {
            rho_on_wall += f[*i];
        }

        let mut rho_normal = 0.0;
        for i in normal_indices.iter() {
            rho_normal += f[*i];
        }

        let vel_normal = (self.ori as f64) * self.u[self.dir as usize];

        (2.0*rho_normal+rho_on_wall) / (1.0+vel_normal)
    }

    /// Computes the momentum from the distribution function 
    /// j = sum_i f_i * c_i
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_j(&self, f: &[f64; Q]) -> [f64; D] {
        let rho = self.compute_rho(f);
        [self.u[0]*rho, self.u[1]*rho]
    }

    /// Returns the stored velocity bc
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations (not used)
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_u(&self, _f: &[f64; Q]) -> [f64; D] {
        self.u.clone()
    }

    /// Computes the stress tensor from the distribution function 
    /// P = sum_i f_i * (c_i-u)*(c_i-u)
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_stress(&self, f: &[f64; Q]) -> [[f64; D]; D] {
        let rho = self.compute_rho(f);

        let p_xx = f[1]+f[2]+f[3]+f[5]+f[6]+f[7]-rho*self.u[0]*self.u[0];
        let p_xy = -f[1]+f[3]-f[5]+f[7]-rho*self.u[0]*self.u[1];
        let p_yy = f[1]+f[3]+f[4]+f[5]+f[7]+f[8]-rho*self.u[1]*self.u[1];

         [[p_xx, p_xy], [p_xy, p_yy]]
    }

    /// Computes the deviatoric stress tensor from the distribution function 
    /// tau = sum_i f_i * (c_i-u)*(c_i-u)-rho*cs2*I
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_deviatoric_stress(&self, f: &[f64; Q]) -> [[f64; D]; D] {
        let rho = self.compute_rho(f);
        let mut stress = self.compute_stress(f);
        stress[0][0] -= rho*CS2;
        stress[1][1] -= rho*CS2;
        stress
    }
}

// // ============= implementation of Dynamics trait for VelDirichletDyn<T> ========= //

impl <T:Dynamics> Dynamics for VelDirichletDyn<T> {
    /// Computes the equilibrium distribution vector at order 2. 
    ///
    /// # Arguments
    ///
    /// * `rho` - the density
    /// * `u` - the velocity
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_equilibria(&self, rho: &f64, u: &[f64; D]) -> [f64; Q] {
        self.base_dyn.compute_equilibria(&rho, &u)
    }

    /// Computes the non-equilibrium distribution vector at order 2. 
    ///
    /// # Arguments
    ///
    /// * `pi` - the deviatoric stress tensor
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_non_equilibria(&self, pi: &[[f64; D]; D]) -> [f64;Q] {
        self.base_dyn.compute_non_equilibria(&pi)
    }

    /// Computes the collision (in-place f^out from f^in). 
    /// Must conserve mass and momentum.
    ///
    /// # Arguments
    ///
    /// * `f` - mutable f^in state
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn collide(&self, f: &mut [f64; Q]) {
        self.base_dyn.collide(f);
    }

    /// Sets the velocity of the population. By default it uses decompose/recompose functions.
    fn set_u(&mut self, _f: &mut [f64; Q], u_new: &[f64; D]) {
        self.u = *u_new;
    }
}



// ========================================================================= //
// ===================== RhoVelDirichletDyn struct ========================= //
// ========================================================================= //

/// The builder class for the RhoVelDirichletDyn.
#[derive(Debug,Clone,Copy)]
pub struct RhoVelDirichletDynBuilder<T> {
    base_dyn: T,
    rho: f64,
    u: [f64; D],
    dir: i32,
    ori: i32,
}


impl <T:Dynamics> RhoVelDirichletDynBuilder<T> {
    pub fn new(base_dyn: T) -> Self {
        RhoVelDirichletDynBuilder{base_dyn: base_dyn, rho: 1.0, u:[0.0, 0.0], dir: 0, ori: 0}
    }

    pub fn set_rho(mut self, rho: f64) -> Self {
        self.rho = rho;
        self
    }

    pub fn set_u(mut self, u: [f64; D]) -> Self {
        self.u = u;
        self
    }

    pub fn set_dir(mut self, dir: i32) -> Self {
        assert!(dir == 0 || dir == 1, "dir must be 0 or 1 and is {:?}", dir);
        self.dir = dir;
        self
    }

    pub fn set_ori(mut self, ori: i32) -> Self {
        assert!(ori == -1 || ori == 1, "dir must be -1 or 1 and is {:?}", ori);
        self.ori = ori;
        self
    }


    pub fn finalize(self) -> RhoVelDirichletDyn<T> {
        assert!(self.dir == 0 || self.dir == 1, "dir must be 0 or 1 and is {:?}", self.dir);
        assert!(self.ori == -1 || self.ori == 1, "dir must be -1 or 1 and is {:?}", self.ori);
        RhoVelDirichletDyn{base_dyn: self.base_dyn, rho: self.rho, u: self.u, dir: self.dir, ori: self.ori}
    }
}

/// The velocity Dirichlet dynamics stores the velocity of its node
/// which is supposed to be a boundary 
/// and computes the density depending on the direction of the boundary.
#[derive(Debug,Clone,Copy)]
pub struct RhoVelDirichletDyn<T> {
    base_dyn: T,
    rho: f64,
    u: [f64; D],
    dir: i32,
    ori: i32,
}

/// Dynamics which stores the velocity. Usefull for Dirichlet boundary conditions.
impl <T:Dynamics> RhoVelDirichletDyn<T> {
    /// By default the velocity is intialized to zero
    pub fn new(base_dyn: T, dir: i32, ori: i32) -> Self {
        RhoVelDirichletDynBuilder::new(base_dyn).set_dir(dir).set_ori(ori).finalize()
    }
}

// ============= implementation of Parameters trait for RhoVelDirichletDyn<T> ========= //
impl <T:Dynamics> Parameters for RhoVelDirichletDyn<T> {
    fn get_rel_freq<'a>(&'a self) -> &'a f64 {
        &self.base_dyn.get_rel_freq()
    }

    fn set_rel_freq(&mut self, omega: f64) {
        self.base_dyn.set_rel_freq(omega);
    }
}

// ============= implementation of Moments trait for RhoVelDirichletDyn<T> ========= //

impl <T:Dynamics> moments::Moments for RhoVelDirichletDyn<T> {
    /// Computes the density from the distribution function 
    /// rho = sum_i f_i
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_rho(&self, _f: &[f64; Q]) -> f64 {
        self.rho
    }

    /// Computes the momentum from the distribution function 
    /// j = sum_i f_i * c_i
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_j(&self, f: &[f64; Q]) -> [f64; D] {
        let rho = self.compute_rho(f);
        [self.u[0]*rho, self.u[1]*rho]
    }

    /// Returns the stored velocity bc
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations (not used)
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_u(&self, _f: &[f64; Q]) -> [f64; D] {
        self.u.clone()
    }

    /// Computes the stress tensor from the distribution function 
    /// P = sum_i f_i * (c_i-u)*(c_i-u)
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_stress(&self, f: &[f64; Q]) -> [[f64; D]; D] {
        let rho = self.compute_rho(f);

        let p_xx = f[1]+f[2]+f[3]+f[5]+f[6]+f[7]-rho*self.u[0]*self.u[0];
        let p_xy = -f[1]+f[3]-f[5]+f[7]-rho*self.u[0]*self.u[1];
        let p_yy = f[1]+f[3]+f[4]+f[5]+f[7]+f[8]-rho*self.u[1]*self.u[1];

         [[p_xx, p_xy], [p_xy, p_yy]]
    }

    /// Computes the deviatoric stress tensor from the distribution function 
    /// tau = sum_i f_i * (c_i-u)*(c_i-u)-rho*cs2*I
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_deviatoric_stress(&self, f: &[f64; Q]) -> [[f64; D]; D] {
        let rho = self.compute_rho(f);
        let mut stress = self.compute_stress(f);
        stress[0][0] -= rho*CS2;
        stress[1][1] -= rho*CS2;
        stress
    }
}

// // ============= implementation of Dynamics trait for RhoVelDirichletDyn<T> ========= //

impl <T:Dynamics> Dynamics for RhoVelDirichletDyn<T> {
    /// Computes the equilibrium distribution vector at order 2. 
    ///
    /// # Arguments
    ///
    /// * `rho` - the density
    /// * `u` - the velocity
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_equilibria(&self, rho: &f64, u: &[f64; D]) -> [f64; Q] {
        self.base_dyn.compute_equilibria(&rho, &u)
    }

    /// Computes the non-equilibrium distribution vector at order 2. 
    ///
    /// # Arguments
    ///
    /// * `pi` - the deviatoric stress tensor
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_non_equilibria(&self, pi: &[[f64; D]; D]) -> [f64;Q] {
        self.base_dyn.compute_non_equilibria(&pi)
    }

    /// Computes the collision (in-place f^out from f^in). 
    /// Must conserve mass and momentum.
    ///
    /// # Arguments
    ///
    /// * `f` - mutable f^in state
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn collide(&self, f: &mut [f64; Q]) {
        self.base_dyn.collide(f);
    }

    /// Sets the deswntiy of the population. By default it uses decompose/recompose functions.
    fn set_rho(&mut self, _f: &mut [f64; Q], rho: &f64) {
        self.rho = *rho;
    }

    /// Sets the velocity of the population. By default it uses decompose/recompose functions.
    fn set_u(&mut self, _f: &mut [f64; Q], u_new: &[f64; D]) {
        self.u = *u_new;
    }
}
