use dynamics::Dynamics;

pub const D:usize = 2;
pub const Q:usize = 9;
pub const CS2: f64 = 1.0/3.0;
pub const INV_CS2: f64 = 3.0;
pub const W:[f64; Q] = [4.0/9.0, 1.0/36.0, 1.0/9.0, 1.0/36.0, 1.0/9.0, 1.0/36.0, 1.0/9.0, 1.0/36.0, 1.0/9.0];
pub const C:[[i32; D]; Q] = [ [0,0], [-1, 1], [-1, 0], [-1,-1], [0,-1], [1,-1], [1, 0], [1, 1], [0, 1] ];

pub mod bulk_dyn;
pub mod bc_dyn;

#[derive(Debug)]
pub struct MutCell<'a> {
    pub dyn: &'a mut Dynamics,
    pub pops: &'a mut [f64; Q],
}

#[derive(Debug)]
pub struct Cell<'a> {
    pub dyn: &'a Dynamics,
    pub pops: &'a [f64; Q],
}

impl <'a> Cell<'a> {
    pub fn compute_rho(&self) -> f64 {
        self.dyn.compute_rho(self.pops)
    }

    pub fn compute_u(&self) -> [f64; D] {
        self.dyn.compute_u(self.pops)
    }

    pub fn get_rel_freq(&'a self) -> &'a f64 {
        &self.dyn.get_rel_freq()
    }

}

impl <'a> MutCell<'a> {
    pub fn collide(&mut self) {
        self.dyn.collide(self.pops);
    }

    pub fn collide_and_swap(&mut self) {
        self.dyn.collide_and_swap(self.pops);
    }

    pub fn regularize(&mut self, rho: &f64, u: &[f64; D], pi: &[[f64; D]; D]) {
        self.dyn.regularize(self.pops, rho, u, pi);
    }

    pub fn set_rho(&mut self, rho: &f64) {
        self.dyn.set_rho(self.pops, rho);
    }

    pub fn set_u(&mut self, u: &[f64; D]) {
        self.dyn.set_u(self.pops, u);
    }

    pub fn set_deviatoric_stress(&mut self, pi_new: &[[f64; D]; D]) {
        self.dyn.set_deviatoric_stress(self.pops, pi_new);
    }
}

// Conditionally compile the module `test` only when the test-suite is run.
#[cfg(test)]
mod test {
    use cell;
    use cell::bulk_dyn::BGK;
    use cell::bc_dyn::BounceBack;
    use dynamics::Dynamics;
    use moments::Moments;
    use std::f64;
    use descriptor2d::Descriptor2d;

    // Kronecker delta function
    fn kronecker(a: usize, b: usize) -> usize {
        if a == b {
            1
        } else {
            0
        }
    }

    // H^{(1)}_i tensor components
    fn h1(i: usize, a: usize) -> f64 {
        cell::C[i][a] as f64
    }

    // H^{(2)}_i tensor components
    fn h2(i: usize, a: usize, b: usize) -> f64 {
        ((cell::C[i][a]*cell::C[i][b]) as f64 - cell::CS2 as f64 * (kronecker(a,b) as f64))
    }

    fn approx_scalar_eq(a: f64, b: f64, eps: f64) -> bool {
        (a-b).abs() <= eps
    }

    fn approx_vec_eq(a: &[f64], b: &[f64], eps: f64) -> bool {
        for i in 0..a.len() {
            if (a[i]-b[i]).abs() > eps {
                return false
            }
        }
        return true
    }

    // ========================================================================
    // ======================= Moments trait tests ===========================
    // ========================================================================
    #[test]
    fn compute_rho_test() {
        let f = cell::W;
        let rho = BGK::default().compute_rho(&f);
        assert!(approx_scalar_eq(rho, 1.0, f64::EPSILON*10.0), 
            "Mass conservation failed: rho = {:?}!", rho);
    }

    #[test]
    fn compute_u_j_test() {
        let f = [0.1, 0.7, 0.2, 0.2, 0.05, 0.073, 0.098, 0.12, 0.38];

        let rho = BGK::default().compute_rho(&f);
        let j = BGK::default().compute_j(&f);
        let u = BGK::default().compute_u(&f);

        let feq = BGK::default().compute_equilibria(&rho, &u);

        let j_eq = BGK::default().compute_j(&feq);
        let u_eq= BGK::default().compute_u(&feq);

        assert!(approx_vec_eq(&u_eq[..], &u[..], f64::EPSILON*10.0), 
            "U computation failed: u = {:?}, u_eq = {:?}!", u, u_eq);
        assert!(approx_vec_eq(&j_eq[..], &j[..], f64::EPSILON*10.0), 
            "J computation failed: j = {:?}, j_eq = {:?}!", j, j_eq);
    }

    // ========================================================================
    // ======================= Dynamics trait tests ===========================
    // ========================================================================
    #[test]
    fn collide_mass_momentum_conservation_test() {
        let mut f = [0.1, 0.7, 0.2, 0.2, 0.05, 0.073, 0.098, 0.12, 0.38];
        let dyn = BGK::new(1.999);
        let rho_in = dyn.compute_rho(&f);
        let j_in = dyn.compute_j(&f);
        dyn.collide(&mut f);
        let rho_out = dyn.compute_rho(&f);
        let j_out = dyn.compute_j(&f);

        assert!(approx_scalar_eq(rho_in, rho_out, f64::EPSILON*10.0), 
            "Mass conservation failed: rho_in = {:?}, rho_out = {:?}!", rho_in, rho_out);
        assert!(approx_vec_eq(&j_in[..], &j_out[..],f64::EPSILON*10.0) , 
            "Momentum conservation failed: j_in = {:?}, j_out = {:?}!", j_in, j_out);
    }

    #[test]
    fn decompose_recompose_test() {
        let rho = 1.0;
        let u = [-0.11, 0.12];
        let pi = [[-0.1, 0.2], [0.2, 0.13]];
        let fneq = [0.0; cell::Q];

        let mut f = [0.0; cell::Q];
        BGK::default().recompose(&mut f, &rho, &u, &pi, &fneq);
        let (rho_t, u_t, pi_t, fneq_t) = BGK::default().decompose(&f);
        assert!(approx_scalar_eq(rho, rho_t, f64::EPSILON*10.0), 
            "Rho decomposition/recomposition failed: rho = {:?}, and rho_t = {:?}!", rho, rho_t);
        assert!(approx_vec_eq(&u[..], &u_t[..],f64::EPSILON*10.0) , 
            "U decomposition/recomposition failed:: u = {:?}, u = {:?}!", u, u_t);

        for i in 0..cell::D {
            assert!(approx_vec_eq(&pi[i], &pi_t[i],f64::EPSILON*10.0) , 
                "Pi[{:?}] decomposition/recomposition failed:: Pi = {:?}, Pi_t = {:?}!", i, pi[i], pi_t[i]);
        }

        assert!(approx_vec_eq(&fneq[..], &fneq_t[..],f64::EPSILON*10.0) , 
            "Fneq decomposition/recomposition failed:: fneq = {:?}, fneq_t = {:?}!", fneq, fneq_t);

        let f2 = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
        let (rho2_t, u2_t, pi2_t, fneq2_t) = BGK::default().decompose(&f2);
        BGK::default().recompose(&mut f, &rho2_t, &u2_t, &pi2_t, &fneq2_t);
        assert!(approx_vec_eq(&f2[..], &f[..],f64::EPSILON*10.0) , 
            "f decomposition/recomposition failed:: f2 = {:?}, f = {:?}!", f2, f);

    }

    // ========================================================================
    // ====================== Descriptor2d trait tests ========================
    // ========================================================================
    #[test]
    fn c_dot_test() {
        let u = [1.1, -2.1];
        let mut h1_dot_u_ref = [0.0; cell::Q];
        for i in 0..cell::Q {
            for a in 0..cell::D {
                h1_dot_u_ref[i] += h1(i,a) * u[a];
            }
        }

        let h1_dot_u = BGK::default().c_dot(&u);
        for i in 0..cell::Q {
            assert!(approx_scalar_eq(h1_dot_u_ref[i], h1_dot_u[i], f64::EPSILON*10.0), "i = {:} failed", i);
        }
    }

    #[test]
    fn h2_dot_test() {
        let pi = [[1.1, -2.1], [-2.1, 4.3]];
        let mut h2_dot_pi_ref = [0.0; cell::Q];
        for i in 0..cell::Q {
            for a in 0..cell::D {
                for b in 0..cell::D {
                    h2_dot_pi_ref[i] += h2(i,a,b) * pi[a][b];
                }
            }
        }

        let h2_dot_pi = BGK::default().h2_dot(&pi);
        for i in 0..cell::Q {
            assert!(approx_scalar_eq(h2_dot_pi_ref[i], h2_dot_pi[i], f64::EPSILON*10.0), "i = {:} failed, pi = {:?} pi_ref = {:?}", i, h2_dot_pi[i], h2_dot_pi_ref[i]);
        }
    }

    // ========================================================================
    // ======================= BounceBack trait tests ===========================
    // ========================================================================


    // ========================================================================
    // ======================= Moments trait tests ===========================
    // ========================================================================
    #[test]
    fn compute_bounce_back_rho_test() {
        let f = cell::W;
        let rho = BounceBack::new().compute_rho(&f);
        assert!(approx_scalar_eq(rho, 1.0, f64::EPSILON*10.0), 
            "Mass conservation failed: rho = {:?}!", rho);
    }

    #[test]
    fn compute_bounce_back_u_j_test() {
        let dummy_f = [0.1, 0.7, 0.2, 0.2, 0.05, 0.073, 0.098, 0.12, 0.38];

        let j = BounceBack::new().compute_j(&dummy_f);

        assert!(approx_vec_eq(&j[..], &[0.0, 0.0], f64::EPSILON*10.0), 
            "U computation failed: j = {:?}, should be = {:?}!", j, [0.0, 0.0]);
    }

    // ========================================================================
    // ======================= Dynamics trait tests ===========================
    // ========================================================================
    #[test]
    fn collide_double_bounceback_test() {
        let mut f = [0.1, 0.7, 0.2, 0.2, 0.05, 0.073, 0.098, 0.12, 0.38];
        let f_bb = [0.1, 0.073, 0.098, 0.12, 0.38, 0.7, 0.2, 0.2, 0.05];
        let dyn = BounceBack::new();
        let f_clone = f.clone();
        dyn.collide(&mut f);
        assert!(approx_vec_eq(&f[..], &f_bb[..],f64::EPSILON*10.0) , 
            "Double bounce back failed: f = {:?}, f_clone = {:?}!", f, f_bb  );
        dyn.collide(&mut f);

        assert!(approx_vec_eq(&f[..], &f_clone[..],f64::EPSILON*10.0) , 
            "DOuble bounce back failed: f = {:?}, f_clone = {:?}!", f, f_clone  );
    }
}
