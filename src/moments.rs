use cell;
use core;

pub trait Moments: 'static+core::fmt::Debug {
	fn compute_rho(&self, f: &[f64; cell::Q]) -> f64;

	fn compute_j(&self, f: &[f64; cell::Q]) -> [f64; cell::D];

	fn compute_u(&self, f: &[f64; cell::Q]) -> [f64; cell::D] {
		let rho = self.compute_rho(f);
		let j = self.compute_j(f);
		[j[0] / rho, (j[1] / rho)]
	}

	fn compute_stress(&self, f: &[f64; cell::Q]) -> [[f64; cell::D];cell::D];

	fn compute_deviatoric_stress(&self, f: &[f64; cell::Q]) -> [[f64; cell::D];cell::D];
}

